//
//  ViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mContainer: UIView!
    @IBOutlet weak var mHomeIconImageView: UIImageView!
    @IBOutlet weak var mFormulaImageView: UIImageView!
    
    @IBOutlet weak var mContentMenuView: UIView!
    @IBOutlet weak var mConceptImageView: UIImageView!
    @IBOutlet weak var mHomeLabel: UILabel!
    @IBOutlet weak var mFormulasLabel: UILabel!
    @IBOutlet weak var mConceptLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setHome()
        //setBottomCorners()
        setColorSelected(mImage: mHomeIconImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulaImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mConceptImageView, mTitle: mConceptLabel)
    }
    
    func setBottomCorners(){
           mContentMenuView.clipsToBounds = true
           mContentMenuView.layer.cornerRadius = 25
           mContentMenuView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func setHome(){
        let news = self.storyboard?.instantiateViewController(withIdentifier: "FormularioViewController") as! FormularioViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChildViewController(news)
        news.didMove(toParentViewController: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
     
    @IBAction func mHomeButton(_ sender: Any) {
        let news = self.storyboard?.instantiateViewController(withIdentifier: "FormularioViewController") as! FormularioViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChildViewController(news)
        news.didMove(toParentViewController: self)
        
        setColorSelected(mImage: mHomeIconImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulaImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mConceptImageView, mTitle: mConceptLabel)
    }
    
    @IBAction func mFormulaButton(_ sender: Any) {
        let news = self.storyboard?.instantiateViewController(withIdentifier: "FormulasViewController") as! FormulasViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChildViewController(news)
        news.didMove(toParentViewController: self)
        setColorDefault(mImage: mHomeIconImageView, mTitle: mHomeLabel)
        setColorSelected(mImage: mFormulaImageView, mTitle: mFormulasLabel)
        setColorDefault(mImage: mConceptImageView, mTitle: mConceptLabel)
    }
    
    @IBAction func mConceptButton(_ sender: Any) {
        let news = self.storyboard?.instantiateViewController(withIdentifier: "CommentsViewController") as! CommentsViewController
        news.view.frame = mContainer.bounds
        mContainer.addSubview(news.view)
        addChildViewController(news)
        news.didMove(toParentViewController: self)
        setColorDefault(mImage: mHomeIconImageView, mTitle: mHomeLabel)
        setColorDefault(mImage: mFormulaImageView, mTitle: mFormulasLabel)
        setColorSelected(mImage: mConceptImageView, mTitle: mConceptLabel)
    }
    
    func setColorDefault(mImage : UIImageView, mTitle: UILabel){
        mTitle.textColor = UIColor.lightGray
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor.lightGray
    }
    
    func setColorSelected(mImage : UIImageView, mTitle: UILabel){
        mTitle.textColor = UIColor(netHex: Colors.color_white)
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_white)
    }
}

