//
//  AlertDialogContinueValidationsViewController.swift
//  mhegas
//
//  Created by Charls Salazar on 5/15/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class AlertDialogContinueValidationsViewController: UIViewController {
    @IBOutlet weak var mDescriptionLabel: UILabel!
    @IBOutlet weak var mDialogContainerView: UIView!
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    var mDescriptionDialog  : String = ""
    var mOpenNewCalculate : String = ""
    var mContinueValidataDelegate : ContinueValidataDelegate!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        mDescriptionLabel.text = mDescriptionDialog
    }

    func getDelegates(mContinueValidataDelegate: ContinueValidataDelegate){
        self.mContinueValidataDelegate = mContinueValidataDelegate
    }
    
    @IBAction func mCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func mContinueButton(_ sender: Any) {
        mContinueValidataDelegate.OnAccept(mViewController:self, mOpenNewCalculate:mOpenNewCalculate)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .default
    }
}

extension AlertDialogContinueValidationsViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}
