//
//  DialogSeconCalculateOperationViewController.swift
//  mhegas
//
//  Created by Charls Salazar on 5/14/19.
//  Copyright © 2019 avento. All rights reserved.
//

import GradientButtonSwift
import UIKit

protocol ShowSpecialDialogDelegate :NSObjectProtocol{
    func OnShowDialog(mViewController: UIViewController, mMessage:  String)
}

class DialogSeconCalculateOperationViewController: UIViewController {
    
    @IBOutlet weak var mCalculateButton: GradientButton!
    @IBOutlet weak var mCLTextField: UITextField!
    @IBOutlet weak var mHCOTextField: UITextField!
    @IBOutlet weak var mNaTextField: UITextField!
    @IBOutlet weak var mDialogContainerView: UIView!
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    var mShowSpecialDialogDelegate : ShowSpecialDialogDelegate!
    var mValidateConditional : ValidateConditionsTable!
    var mFormValidator : FormValidator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        mValidateConditional = ValidateConditionsTable()
        setFormValidator()
    }
    
    func setFormValidator(){
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
            TextFieldValidator(textField : mCLTextField, regex: RegexEnum.NOT_EMPTY, messageError: "Es necesario ingresar un nombre"),
                                     TextFieldValidator(textField : mHCOTextField, regex: RegexEnum.NOT_EMPTY, messageError: "Es necesario llenar todos los campos"),
                                     TextFieldValidator(textField : mNaTextField, regex: RegexEnum.NOT_EMPTY, messageError: "Es necesario llenar todos los campos")
        )
    }
    
    func getDelegates(mShowSpecialDialogDelegate : ShowSpecialDialogDelegate){
        self.mShowSpecialDialogDelegate = mShowSpecialDialogDelegate
    }
    
    @IBAction func mCalculateButton(_ sender: Any) {
        if(mFormValidator.isValid()){
            evaluateConditions()
        }else{
            launchDialog(mMessage: "Es necesario todos los campos")
        }
    }
    
    func launchDialogError(mMessage : String) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: AlertViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
        v1.mLegend = mMessage
        MIBlurPopup.show(v1, on: self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .lightContent
    }
    
    func evaluateConditions(){
        let mCl = mCLTextField.text?.toDouble()
        let mHCO = mHCOTextField.text?.toDouble()
        let mNA = mNaTextField.text?.toDouble()
        
        let mResult = mValidateConditional.AnionGap(Na: mNA!, Cl: mCl!, HCO3: mHCO!)
        if(mResult > 12){
            launchDialog(mMessage: StringDescriptions.description_result_eleven)
        }else{
            if(mResult < 12){
                launchDialog(mMessage: StringDescriptions.description_result_twelve)
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mCalculateButton.applyGradient(colors: [UIColor.init(red: 84, green: 164 , blue: 26).cgColor, UIColor.init(red: 54, green: 110, blue: 16).cgColor])
        mCalculateButton.clipsToBounds = true
    }
    
    func launchDialog(mMessage: String){
        mShowSpecialDialogDelegate.OnShowDialog(mViewController: self, mMessage: mMessage)
    }
}


extension DialogSeconCalculateOperationViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}
