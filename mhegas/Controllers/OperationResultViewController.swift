//
//  OperationResultViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/29/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class OperationResultViewController: UIViewController {
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    
    @IBOutlet weak var mResultValueLabel: UILabel!
    @IBOutlet weak var mDialogContainerView: UIView!
    @IBOutlet weak var mDescriotionLabel: UILabel!
    
    @IBOutlet weak var mHeightDescriptionLabel: NSLayoutConstraint!
    @IBOutlet weak var mDescriptionResultLabel: UILabel!
    @IBOutlet weak var mTitleRecomendationLabel: UILabel!
    var mResult : String?
    var mFormula : String?
    
    @IBOutlet weak var mHeightViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var mEqualImageView: UIImageView!
    override func viewDidLoad() {
        modalPresentationCapturesStatusBarAppearance = true
        mResultValueLabel.text = mResult
        if(mFormula != nil || !mFormula!.isEmpty){
            validateSetRecommendation()
        }
        
        setColorDefault(mImage: mEqualImageView)
    }
    
    func setColorDefault(mImage : UIImageView){
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_white)
    }
    
    func setSize(){
        let cHeight = requiredHeight(labelText: mDescriotionLabel.text!)
        
        if ((requiredHeight(labelText: mDescriotionLabel.text!)) > 400 ){
            mHeightViewConstraint.constant =  cHeight + 100
            mHeightDescriptionLabel.constant = requiredHeight(labelText: mDescriotionLabel.text!) - 100
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .default
    }

    @IBAction func mAcceptButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func validateSetRecommendation() {
        switch (mFormula) {
        case "concentracionPlasmatica":
            mDescriotionLabel.text = "Normal = 40 ± 5"
            break
            
        case "acidosisMetabolica":
            //Nothing to show
            break
            
        case "alcalosisMetabolica":
            //Nothing to show
            break
            
        case "anion":
            mDescriotionLabel.text = "Valor normal  3 - 11 mEq/L"
            break
            
        case "anionGap":
            //Nothing to show
            break
            
        case "osmolalidad":
           //Nothing to show
            break
            
        case "deficitBicarbonato":
            mDescriotionLabel.text = "Cálculo de la reposición de bicarbonato"
            break
            
        case "deficitCI":
            //Nothing to show
            break
            
        case "defitcitHplus":
            //Nothing to show
            break
            
        case "hiatoAnionico":
            let mRes = mResult!.toDouble()
            if(Double(mRes!) > 1.0){
                mDescriotionLabel.text = "Significa Alcalosis metabólica agregada (ej. cetoacidosis diabética más uso de diuréticos o aspiración gástrica)"
            }else{
                 mDescriotionLabel.text = "Significa Acidosis metabólica de hiato aniónico normal agregada (ej. cetoacidosis diabética más acidosis hiperclorémica agregada)."
            }
            break
        default: break
        }
    }
    
    func hiddenRecommendation(){
        
    }
    
    func requiredHeight(labelText:String) -> CGFloat {
        
        let font = UIFont(name: "Helvetica", size: 16.0)
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: .max))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = labelText
        label.sizeToFit()
        return label.frame.height
        
    }

}

extension OperationResultViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}

