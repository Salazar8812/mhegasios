//
//  CalculateOperation.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/26/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class CalculateOperation: NSObject {
   /**
     Concentración plasmática de Hidrogeniones
 **/
    
    func concentracionPlasmatica(pCO2 : Double, HCO3 : Double)->Double{
        let mAux = (pCO2 / HCO3)
        let mResult = 23.9 * mAux
        
        return mResult
    }
    
    /**
     PCO2 esperado para Acidosis Metabólica
 **/
    
    func acidosisMetabolica(HCO3 : Double)-> Double{
        let mAux = HCO3  * 1.5
        let mResult = mAux + 8
        return mResult
    }
    
    /**
     PCO2 esperado para Alcalosis Metabólica
     **/
    
    func alcalosisMetabolica(HCO3 : Double)-> Double{
        let mAux = HCO3  * 0.7
        let mResult = mAux + 21
        return mResult
    }
    
    /**
      PCO2 esperado para Alcalosis Metabólica.
     **/
    
    func anion(Na: Double, Cl: Double, HCO3 : Double)->Double{
        let mAux = Cl + HCO3
        let mResult = Na - mAux
        return mResult
    }
    
    /**
     Anion Gap corregido en hipoalbuminemia.
     **/

    func anionGap(albuminaDL : Double, anionGap : Double ) -> Double{
        let mAux = anionGap + 2.5
        let mNAux = 4.5 - albuminaDL
        let mResult = mAux * mNAux
        
        return mResult
    }
    
    
    func osmolalidad(glucosa: Double , Na: Double )->Double{
        let mAux = 2 * Na
        let mSAux = glucosa / 18
        let mResult = mAux + mSAux
        
        return mResult
    }
    
    /**
     Cálculo de la reposición de bicarbonato
 **/
    
    func deficitBicarbonato(bNormal : Double , bMedido: Double, mPeso : Double) -> Double{
        let mAux = (24 - bMedido) * 0.4 * mPeso
        return mAux
    }
    
    func deficitCI(mPeso : Double , CIPlasmatico : Double) -> Double{
        let mAux = 0.2 * mPeso
        let mSAux = 100 - CIPlasmatico
        let mResult = mAux * mSAux
        return mResult
    }
    
    func defitcitHplus(mPeso : Double, HCO3 : Double)->Double{
        let mAux = 0.5 * mPeso
        let mSAux = HCO3 - 30
        let mResult = mAux * mSAux
        return mResult
    }
    
    func hiatoAnionico(hiatoAnionico : Double, HCO3 : Double ) -> Double{
        let mAux = hiatoAnionico - 12
        let mSAux = 24 - HCO3
        let mResult = mAux / mSAux
        
        return mResult
    }
    
}
