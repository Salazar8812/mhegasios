//
//  CommentsViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class CommentsViewController: UIViewController, TableViewCellClickDelegate {
    
    @IBOutlet weak var mSearchImageView: UIImageView!
    @IBOutlet weak var mListConceptTableView: UITableView!
    @IBOutlet weak var mSearchTextField: UITextField!
    var mDataSource : BaseDataSource<NSObject, ItemConceptTableViewCell>?
    var mListFomulas : [Formulas] = []
    var mListAux : [Formulas] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateFormulas()
        mDataSource = BaseDataSource(tableView: self.mListConceptTableView, delegate: self)
        mDataSource?.setHeightRow(height:65)
        mDataSource?.update(items: mListFomulas)
        
        addTarget()
        setTintcolor()
    }
    
    func setTintcolor(){
        mSearchImageView.image = mSearchImageView.image?.withRenderingMode(.alwaysTemplate)
        //mSearchImageView.tintColor = UIColor(netHex: Colors.color_background_app)
    }
    
    func addTarget(){
        mSearchTextField.addTarget(self, action: #selector(watcher(_:)), for: .editingChanged)
    }
    
    @objc func watcher(_ textfield : UITextField){
        var cadAux : String = ""
        cadAux = textfield.text!
        mListAux.removeAll()
        if(cadAux.count == 0 || cadAux == ""){
            mDataSource!.update(items: mListFomulas)
        }else{
            for item in mListFomulas{
                if(item.mTipoFormula.uppercased().contains(cadAux.uppercased())){
                    mListAux.append(item)
                }
            }
            mDataSource!.update(items: mListAux)
        }
    }
    
    func populateFormulas(){
        mListFomulas.append(Formulas(mTipoFormula: "pH normal", mNombreFormula: "Peso ideal (Pacientes sin SDRA)", mIdFormula: "1", mParams: ["Talla (cm)2 *"],mFunctionCalculate: "concentracionPlasmatica", mDescription: StringDescriptions.description_form1));
        mListFomulas.append(Formulas(mTipoFormula: "Dióxido de carbono (pCO2) arterial normal", mNombreFormula: "Peso predicho para SDRA (ARDSnet)", mIdFormula: "1",mParams: ["Talla (cm) *"],mFunctionCalculate: "acidosisMetabolica",mDescription: StringDescriptions.description_form2));
        mListFomulas.append(Formulas(mTipoFormula: "Bicarbonato (HCO3–) normal", mNombreFormula: "IMC", mIdFormula: "1",mParams: ["Peso (Kg) *", "(m)2 *"],mFunctionCalculate: "alcalosisMetabolica",mDescription: StringDescriptions.description_form3));
        //nuevas
        mListFomulas.append(Formulas(mTipoFormula: "Cambio en el pH = Trastorno ácido-base", mNombreFormula: "CO2e para Acidosis Metabólica", mIdFormula: "1",mParams: ["Vt (ml) *","Pmeseta *","PEEP *"],mFunctionCalculate: "anion", mDescription: StringDescriptions.description_form4));
        mListFomulas.append(Formulas(mTipoFormula: "Cambio en la PCO2 = Trastorno ácido-base respiratorio", mNombreFormula: "Distensibilidad pulmonar dinámica (Cdyn compliance dinámica)", mIdFormula: "1",mParams: ["Vt (ml) *","Pmax *","PEEP *"],mFunctionCalculate: "anionGap", mDescription: StringDescriptions.description_form5));
        mListFomulas.append(Formulas(mTipoFormula: "Cambio en el HCO3: Trastorno ácido-base metabolico", mNombreFormula: "Presión de Vapor de Agua [P H2O]a", mIdFormula: "1",mParams: ["Pmax *","Pmeseta *"],mFunctionCalculate: "osmolalidad", mDescription: StringDescriptions.description_form6));
        mListFomulas.append(Formulas(mTipoFormula: "Por cada 0,1 unidades que aumenta el pH plasmático, la [K+ ]p disminuye en 0,6 mmol/l, y viceversa", mNombreFormula: "Presión parcial de O2 Arterial (PaO2)", mIdFormula: "1",mParams: ["Pmax *","Pmeseta *", "Flujo *"],mFunctionCalculate: "deficitBicarbonato", mDescription: StringDescriptions.description_form7));
        
        mListFomulas.append(Formulas(mTipoFormula: "Presión Inspirada de O2 (PIO2)", mNombreFormula: "Presión Inspirada de O2 (PIO2)", mIdFormula: "1",mParams: ["HCO3 *"],mFunctionCalculate: "deficitCI",mDescription: StringDescriptions.description_form8));
        mListFomulas.append(Formulas(mTipoFormula: "Presión Alveolar de O2 (PAO2)", mNombreFormula: "Presión Alveolar de O2 (PAO2)", mIdFormula: "1",mParams: ["HCO3 *"],mFunctionCalculate: "defitcitHplus",mDescription: StringDescriptions.description_form9));
        mListFomulas.append(Formulas(mTipoFormula: "Diferencia Alveolo-arterial de O2 [P (A-a)O2]", mNombreFormula: "Diferencia Alveolo-arterial de O2 [P (A-a)O2]", mIdFormula: "1",mParams: ["Fr *","paCO2 *","CO2e *"],mFunctionCalculate: "hiatoAnionico", mDescription: StringDescriptions.description_form10));
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        let mFormula = item as! Formulas
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: DescriptionFormulasViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "DescriptionFormulasViewController") as! DescriptionFormulasViewController
        v1.mTitle = mFormula.mTipoFormula
        v1.mContentDescription = mFormula.mDescription
        MIBlurPopup.show(v1, on: self)
    }

}
