//
//  AnimateSendEmailViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/2/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit
import FLAnimatedImage

class AnimateSendEmailViewController: UIViewController , FinishDelegate {
   
    
    
    @IBOutlet weak var mDialogContainerView: UIView!
    @IBOutlet weak var mSendEmailImageView: FLAnimatedImageView!
    
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    var counter = 0
    var timer = Timer()
    var mFinishDdelegate : FinishDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        loadGifsProgress()
        delayLaunchIntroductionView()
    }
    
    func OnFinish(mViewController: UIViewController) {
        mViewController.dismiss(animated: true, completion: nil)
    }
    
    func getDelegate(mFinishDelegate : FinishDelegate){
        self.mFinishDdelegate = mFinishDelegate
    }
    
    func loadGifsProgress(){
        let imageData = try! Data(contentsOf: Bundle.main.url(forResource: "sendingmail", withExtension: "gif")!)
        mSendEmailImageView.animatedImage = FLAnimatedImage(animatedGIFData: imageData)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .lightContent
    }
    
    func delayLaunchIntroductionView(){
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(actionDelayHome), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelayHome() {
        counter += 1
        if(counter == 10){
            timer.invalidate()
            mFinishDdelegate.OnFinish(mViewController: self)
        }
    }
    
}

extension AnimateSendEmailViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}
