//
//  CalculateNewOperationViewController.swift
//  mhegas
//
//  Created by Charls Salazar on 5/13/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit
import GradientButtonSwift

class CalculateNewOperationViewController: UIViewController , ContinueValidataDelegate, ShowSpecialDialogDelegate{
    
    @IBOutlet weak var mFieldOneTextiField: UITextField!
    
    @IBOutlet weak var mFieldTwoTextField: UITextField!
    
    @IBOutlet weak var mFieldThreeTextField: UITextField!
    
    @IBOutlet weak var mFieldFourTextField: UITextField!
    
  
    @IBOutlet weak var mCalculateButton: GradientButton!
    var mValidateConditions: ValidateConditionsTable?
    
    var  mFormValidator : FormValidator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mValidateConditions = ValidateConditionsTable()
        setFormat()
    }
    
    func setFormat(){
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
            TextFieldValidator(textField : mFieldOneTextiField, regex: RegexEnum.NOT_EMPTY, messageError: "Es necesario ingresar un nombre"),
                                     TextFieldValidator(textField : mFieldTwoTextField, regex: RegexEnum.NOT_EMPTY, messageError: "Es necesario llenar todos los campos"),
                                     TextFieldValidator(textField : mFieldThreeTextField, regex: RegexEnum.NOT_EMPTY, messageError: "Es necesario llenar todos los campos"),
                                     TextFieldValidator(textField : mFieldFourTextField, regex: RegexEnum.NOT_EMPTY, messageError: "Es necesario llenar todos los campos")
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(netHex: Colors.color_white)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "Calculador de operaciones"
        navigationController?.navigationBar.backgroundColor = UIColor(netHex: Colors.color_green_background) //UIColor.init(red: 9, green: 26, blue: 160)
        navigationController?.navigationBar.barTintColor = UIColor(netHex: Colors.color_green_strong)  // UIColor.init(red: 9, green: 26, blue: 160)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        mCalculateButton.applyGradient(colors: [UIColor.init(red: 84, green: 164 , blue: 26).cgColor, UIColor.init(red: 54, green: 110, blue: 16).cgColor])
        mCalculateButton.clipsToBounds = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func launchDialog(mMessage : String, mContinueValidation: String) {
        if(mContinueValidation == "true"){
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: AlertSpecialValidationsViewController.self))
            
            let v1 = storyboard.instantiateViewController(withIdentifier: "AlertDialogContinueValidationsViewController") as! AlertDialogContinueValidationsViewController
            v1.mDescriptionDialog = mMessage
            v1.mOpenNewCalculate = mContinueValidation
            v1.getDelegates(mContinueValidataDelegate: self)
            MIBlurPopup.show(v1, on: self)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: AlertSpecialValidationsViewController.self))
        
            let v1 = storyboard.instantiateViewController(withIdentifier: "AlertSpecialValidationsViewController") as! AlertSpecialValidationsViewController
            v1.mDescriptionDialog = mMessage
            v1.mOpenNewCalculate = mContinueValidation
            v1.getDelegates(mContinueValidataDelegate: self)
            MIBlurPopup.show(v1, on: self)
        }
    }
    
    @IBAction func mCalculateButton(_ sender: Any) {
        if(mFormValidator.isValid()){
            validateConditions()
        }else{
            launchDialogError(mMessage: "Es necesario ingresar toda la información")
        }
    }
    
    func launchDialogError(mMessage : String) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: AlertViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
        v1.mLegend = mMessage
        MIBlurPopup.show(v1, on: self)
    }
    
    func launchContinueCalculate() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: DialogSeconCalculateOperationViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "DialogSeconCalculateOperationViewController") as! DialogSeconCalculateOperationViewController
        v1.getDelegates(mShowSpecialDialogDelegate: self)
        MIBlurPopup.show(v1, on: self)
    }
    
    func OnAccept(mViewController: UIViewController, mOpenNewCalculate: String) {
        mViewController.dismiss(animated: true, completion: nil)
        if(mOpenNewCalculate == "true"){
            launchContinueCalculate()
        }else{
            
        }
    }
    
    func OnShowDialog(mViewController: UIViewController, mMessage: String) {
        mViewController.dismiss(animated: true, completion: nil)
        launchDialog(mMessage: mMessage, mContinueValidation: "false")
    }
    
    func validateConditions(){
        let mFieldOne = mFieldOneTextiField.text!.toDouble()
        let mFieldTwo = mFieldTwoTextField.text!.toDouble()
        let mFieldThree = mFieldThreeTextField.text!.toDouble()
        let mFieldFour = mFieldFourTextField.text!.toDouble()
        
        let mFormOne = mValidateConditions?.PCO2Esperado(HCO3:mFieldFour!)
        let mFormTwo = mValidateConditions?.BaseEsperada(PaCO2: mFieldTwo!)
        
        
        if (Double(mFieldOne!) < 7.35 && Double(mFieldTwo!) < 35.0 && Double(mFieldTwo!) < Double(mFormOne!)) {
            launchDialog(mMessage: StringDescriptions.description_result_one, mContinueValidation: "true")
        } else {
            if (Double(mFieldOne!) < 7.35 && Double(mFieldTwo!) < 35.0 && Double(mFieldTwo!) > Double(mFormOne!)) {
                launchDialog(mMessage: StringDescriptions.description_result_two, mContinueValidation: "true")
            }else{
                if (Double(mFieldOne!) > 7.45 && Double(mFieldTwo!) > 45.0 && Double(mFieldTwo!) < Double(mFormOne!)) {
                    launchDialog(mMessage: StringDescriptions.description_result_three, mContinueValidation: "false")
                }else{
                    if (Double(mFieldOne!) > 7.45 && Double(mFieldTwo!) > 45.0 && Double(mFieldTwo!) > Double(mFormOne!)) {
                        launchDialog(mMessage: StringDescriptions.description_result_four, mContinueValidation: "false")
                    }else{
                        if (Double(mFieldOne!) < 7.35 && Double(mFieldTwo!) > 45.0 && Double(mFieldThree!) > -2.1 && Double(mFieldThree!) < 2.1) {
                            launchDialog(mMessage: StringDescriptions.description_result_five, mContinueValidation: "false")
                        }else{
                            if (Double(mFieldOne!) < 7.35 && Double(mFieldTwo!) > 45.0 && Double(mFieldThree!) > Double(mFormTwo!)) {
                                launchDialog(mMessage: StringDescriptions.description_result_six, mContinueValidation: "false")
                            }else{
                                if (Double(mFieldOne!) < 7.35 && Double(mFieldTwo!) > 45.0 && Double(mFieldThree!) < Double(mFormTwo!)) {
                                    launchDialog(mMessage: StringDescriptions.description_result_seven, mContinueValidation: "false")
                                }else{
                                    if (Double(mFieldOne!) > 7.35 && Double(mFieldTwo!) < 45.0 && Double(mFieldThree!) > -2.1 && Double(mFieldThree!) < 2.1) {
                                        launchDialog(mMessage: StringDescriptions.description_result_eight, mContinueValidation: "false")
                                    }else{
                                        if (Double(mFieldOne!) > 7.45 && Double(mFieldTwo!) < 35.0 && Double(mFieldThree!) > Double(mFormTwo!)) {
                                            launchDialog(mMessage: StringDescriptions.description_result_nine, mContinueValidation: "false")
                                        }else{
                                            if (Double(mFieldOne!) > 7.45 && Double(mFieldTwo!) < 35.0 && Double(mFieldThree!) < Double(mFormTwo!)) {
                                                launchDialog(mMessage: StringDescriptions.description_result_ten, mContinueValidation: "false")
                                            }else{
                                                if(Double(mFieldOne!) > 7.34 && Double(mFieldOne!) < 7.46 && Double(mFieldTwo!) > 34  && Double(mFieldTwo!) < 46 && Double(mFieldThree!) > -2 && Double(mFieldThree!) < 3 && Double(mFieldFour!) > 21 && Double(mFieldFour!) < 27) {
                                                    launchDialog(mMessage: StringDescriptions.description_result_ten, mContinueValidation: "false")
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
}


