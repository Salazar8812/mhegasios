//
//  ContactViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit
import MessageUI
import GradientButtonSwift

class ContactViewController: UIViewController, MFMailComposeViewControllerDelegate, FinishDelegate {
   
    @IBOutlet weak var mTwitterImageView: UIImageView!
    @IBOutlet weak var mFacebookImageView: UIImageView!
    @IBOutlet weak var mInstagramImageView: UIImageView!
    @IBOutlet weak var sendButton: GradientButton!
    @IBOutlet weak var mUserImageView: UIImageView!
    @IBOutlet weak var mNameTextField: UITextField!
    @IBOutlet weak var mEmailTextField: UITextField!
    @IBOutlet weak var mCommentTextField: UITextField!
    @IBOutlet weak var mEmailImageView: UIImageView!
    @IBOutlet weak var mCommentImageView: UIImageView!
    var mFormValidator : FormValidator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setColorDefault(mImage: mTwitterImageView)
        setColorDefault(mImage: mFacebookImageView)
        setColorDefault(mImage: mInstagramImageView)
        
        setColorIconForms(mImage:mUserImageView)
        setColorIconForms(mImage: mEmailImageView)
        setColorIconForms(mImage:mCommentImageView)
        
        setFormat()
            
    }
    
    func setFormat(){
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
        TextFieldValidator(textField : mNameTextField, regex: RegexEnum.NOT_EMPTY, messageError: "Es necesario ingresar un nombre"),
                                     TextFieldValidator(textField : mCommentTextField, regex: RegexEnum.NOT_EMPTY, messageError: "Es necesario llenar todos los campos")
        )
    }
    
    func setFormatEmail(){
          mFormValidator.addValidators(validators:TextFieldValidator(textField : mEmailTextField, regex: RegexEnum.EMAIL, messageError: "Ingrese una dirección válida"))
    }
    
    @IBAction func mSendEmailButton(_ sender: Any) {
        
        if(mFormValidator.isValid()){
            setFormatEmail()
            if(mFormValidator.isValid()){
                sendEmail()
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: AnimateSendEmailViewController.self))
                let v1 = storyboard.instantiateViewController(withIdentifier: "AnimateSendEmailViewController") as! AnimateSendEmailViewController
                v1.getDelegate(mFinishDelegate: self)
                MIBlurPopup.show(v1, on: self)
            }else{
                 setFormat()
                 AlertDialog.show(title: "Aviso", body: "Dirección de email no válida", view: self)
            }
        }else{
            AlertDialog.show(title: "Aviso", body: "Es necesario ingresar los datos en todos los campos", view: self)
        }
    }

    
    func setColorIconForms(mImage : UIImageView){
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_green_strong)
    }
    
    
    func sendEmail(){
        let smtpSession = MCOSMTPSession()
        smtpSession.hostname = "smtp.gmail.com"
        smtpSession.username = "aventho240419@gmail.com"
        smtpSession.password = "appaventho"
        smtpSession.port = 465
        smtpSession.authType = MCOAuthType.saslPlain
        smtpSession.connectionType = MCOConnectionType.TLS
        smtpSession.connectionLogger = {(connectionID, type, data) in
            if data != nil {
                if let string = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                    NSLog("Connectionlogger: \(string)")
                }
            }
        }
        
        let builder = MCOMessageBuilder()
        builder.header.to = [MCOAddress(displayName: "Para:", mailbox: "aventho240419@gmail.com")]
        builder.header.from = MCOAddress(displayName: "De:", mailbox: mEmailTextField.text)
        builder.header.subject = mNameTextField.text
        builder.htmlBody  = "\(mCommentTextField.text!) \r\n" + "De: \(mEmailTextField.text!)"
        
        let rfc822Data = builder.data()
        let sendOperation = smtpSession.sendOperation(with: rfc822Data)
        sendOperation!.start { (error) -> Void in
            if (error != nil) {
                NSLog("Error sending email: \(String(describing: error))")
            } else {
                NSLog("Successfully sent email!")
            }
        }
    }
    
    func OnFinish(mViewController: UIViewController) {
        mViewController.dismiss(animated: true, completion: nil)
        mNameTextField.text = ""
        mEmailTextField.text = ""
        mCommentTextField.text = ""
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: SuccessSendEmailViewController.self))
        let v1 = storyboard.instantiateViewController(withIdentifier: "SuccessSendEmailViewController") as! SuccessSendEmailViewController
        MIBlurPopup.show(v1, on: self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sendButton.applyGradient(colors: [UIColor.init(red: 84, green: 164 , blue: 26).cgColor, UIColor.init(red: 54, green: 110, blue: 16).cgColor])
        sendButton.clipsToBounds = true
    }
    
    func setColorDefault(mImage : UIImageView){
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_yellow)
    }
    
    @IBAction func mInstagramButton(sender: AnyObject) {
        callURL(mURL: "https://www.instagram.com/aventho_ventilacion/")
    }
    
    @IBAction func mFacebookButton(sender: AnyObject) {
        callURL(mURL: "https://www.facebook.com/AVENTHO/")
    }
    
    @IBAction func mTwitterButton(sender: AnyObject) {
        callURL(mURL: "https://twitter.com/vm_aventho")
    }
    
    func callURL(mURL : String){
        let url = URL(string: mURL)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            //If you want handle the completion block than
            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                print("Open url : \(success)")
            })
        }
    }
    
    func OnFinish() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: SuccessSendEmailViewController.self))
        let v1 = storyboard.instantiateViewController(withIdentifier: "SuccessSendEmailViewController") as! SuccessSendEmailViewController
        MIBlurPopup.show(v1, on: self)
    }
}

