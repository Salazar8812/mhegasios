//
//  FormsCalculateViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/11/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit
import DLRadioButton
import GradientButtonSwift

class FormsCalculateViewController: UIViewController {
    var mResult : Double = 0.0
    var formSelected : String = ""
    var mFormulas : Formulas!
    @IBOutlet weak var mContentFieldThree: UIView!
    @IBOutlet weak var mDetailFormula: UILabel!
    @IBOutlet weak var mHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var act_form_name: UILabel!
    @IBOutlet weak var mContentFieldFour: UIView!
    @IBOutlet weak var mContentFieldTwo: UIView!
    @IBOutlet weak var mEditFieldOne: UITextField!
    @IBOutlet weak var mEditFieldThree: UITextField!
    @IBOutlet weak var mEditFieldFour: UITextField!
    @IBOutlet weak var mFieldOne: UILabel!
    @IBOutlet weak var mEditFieldTwo: UITextField!
    @IBOutlet weak var mContentFieldOne: UIView!
    @IBOutlet weak var mFieldFour: UILabel!
    @IBOutlet weak var mFieldThree: UILabel!
    @IBOutlet weak var mFieldTwo: UILabel!
    var mCalculateOperation : CalculateOperation!
    
    @IBOutlet weak var mWomanRadioButton: DLRadioButton!
    @IBOutlet weak var mMenRadioButton: DLRadioButton!
    
    @IBOutlet weak var calcularButtom: UIButton!
    
    var mListLinear : [itemViews] = []
    
    var mMan: Bool = false
    var mWoman : Bool = false
    
    @IBOutlet weak var mContentRadioGroup: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mCalculateOperation = CalculateOperation()
        formSelected = mFormulas.mFunctionCalculate
        setColors()
        hideAndShow()
        if(mFormulas.mDescription.isEmpty || mFormulas.mDescription == ""){
        }else{
            mDetailFormula.text = mFormulas.mDescription
        }
        act_form_name.text = mFormulas.mNombreFormula
        if (mFormulas.mFunctionCalculate.contains("CalculateWeightManOutSDRA") || mFormulas.mFunctionCalculate.contains("CalculateWeightManWithSDRA")) {
            ViewUtils.setVisibility(view: mContentRadioGroup, visibility: .VISIBLE)
            mHeightConstraint.constant = 55
            mContentRadioGroup.layoutIfNeeded()
        }else{
            ViewUtils.setVisibility(view: mContentRadioGroup, visibility: .INVISIBLE)
            mHeightConstraint.constant = 0
            mContentRadioGroup.layoutIfNeeded()
        }
    }
    
    func hideAndShow(){
        setList()
        let size = mListLinear.count - mFormulas.mParams.count
        
        if(size > 0){
            for index in 0...size-1 {
                ViewUtils.setVisibility(view: mListLinear[index].mContainer, visibility: .INVISIBLE)
            }
        }
        
        populateInfo()
    }
    
    func populateInfo(){
        revertList()
        for index in 0...mFormulas.mParams.count-1 {
            mListLinear[index].mLabel.text = mFormulas.mParams[index]
            mListLinear[index].mTextField.placeholder = mFormulas.mParams[index]
        }
    }
    
    func revertList(){
        mListLinear.removeAll()
        mListLinear.append(itemViews(mContainer: mContentFieldOne, mLabel: mFieldOne, mTextField: mEditFieldOne))
        mListLinear.append(itemViews(mContainer: mContentFieldTwo, mLabel: mFieldTwo, mTextField: mEditFieldTwo))
        mListLinear.append(itemViews(mContainer: mContentFieldThree, mLabel: mFieldThree, mTextField: mEditFieldThree))
        mListLinear.append(itemViews(mContainer: mContentFieldFour, mLabel: mFieldFour, mTextField: mEditFieldFour))
    }
    
    func setList(){
        mListLinear.removeAll()
        mListLinear.append(itemViews(mContainer: mContentFieldFour, mLabel: mFieldFour, mTextField: mEditFieldFour))
        mListLinear.append(itemViews(mContainer: mContentFieldThree, mLabel: mFieldThree, mTextField: mEditFieldThree))
        mListLinear.append(itemViews(mContainer: mContentFieldTwo, mLabel: mFieldTwo, mTextField: mEditFieldTwo))
        mListLinear.append(itemViews(mContainer: mContentFieldOne, mLabel: mFieldOne, mTextField: mEditFieldOne))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(netHex: Colors.color_white)

        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "Calculador de operaciones"
        
        navigationController?.navigationBar.barTintColor = UIColor(netHex: Colors.color_red)  // UIColor.init(red: 9, green: 26, blue: 160)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        //calcularButtom.applyGradient(colors: [UIColor.init(red: 84, green: 164 , blue: 26).cgColor, UIColor.init(red: 54, green: 110, blue: 16).cgColor])
        calcularButtom.clipsToBounds = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func mCalculateOperationButton(_ sender: Any) {
        selectFormula()
    }
    
    func launchDialog() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: OperationResultViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "OperationResultViewController") as! OperationResultViewController
        v1.mResult = mResult.toString()
        v1.mFormula = formSelected
        MIBlurPopup.show(v1, on: self)
    }
    
    func launchDialogError(mMessage : String) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: AlertViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
        v1.mLegend = mMessage
        MIBlurPopup.show(v1, on: self)
    }
    
    func selectFormula() {
        
        switch (mFormulas.mFunctionCalculate) {
        case "concentracionPlasmatica":
            if(!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty){
                mResult = mCalculateOperation.concentracionPlasmatica(pCO2: (mEditFieldOne.text?.toDouble())!, HCO3: (mEditFieldTwo.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
            
        case "acidosisMetabolica":
            if(!mEditFieldOne.text!.isEmpty){
                mResult = mCalculateOperation.acidosisMetabolica(HCO3: (mEditFieldOne!.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
            
        case "alcalosisMetabolica":
            if(!mEditFieldOne.text!.isEmpty){
                mResult = mCalculateOperation.alcalosisMetabolica(HCO3: (mEditFieldOne!.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
            
        case "anion":
            if(!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty){
                mResult = mCalculateOperation.anion(Na: (mEditFieldOne.text?.toDouble())!, Cl: (mEditFieldTwo.text?.toDouble())!, HCO3: (mEditFieldThree.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
            
        case "anionGap":
            if(!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty){
                mResult = mCalculateOperation.anionGap(albuminaDL: (mEditFieldOne.text?.toDouble())!, anionGap: (mEditFieldThree.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
            
        case "osmolalidad":
            if(!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty){
                mResult = mCalculateOperation.osmolalidad(glucosa: (mEditFieldOne.text?.toDouble())!, Na: (mEditFieldTwo.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
            
        case "deficitBicarbonato":
            if(!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty && !mEditFieldThree.text!.isEmpty){
                mResult = mCalculateOperation.deficitBicarbonato(bNormal: (mEditFieldOne.text?.toDouble())!, bMedido: (mEditFieldTwo.text?.toDouble())!, mPeso: (mEditFieldThree.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
            
        case "deficitCI":
            if(!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty){
                mResult = mCalculateOperation.deficitCI(mPeso: (mEditFieldOne.text?.toDouble())!, CIPlasmatico: (mEditFieldTwo.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
            
        case "defitcitHplus":
            if(!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty){
                mResult = mCalculateOperation.defitcitHplus(mPeso: (mEditFieldOne.text?.toDouble())!, HCO3: (mEditFieldTwo.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
            
        case "hiatoAnionico":
            if(!mEditFieldOne.text!.isEmpty && !mEditFieldTwo.text!.isEmpty){
                mResult = mCalculateOperation.hiatoAnionico(hiatoAnionico: (mEditFieldOne.text?.toDouble())!, HCO3: (mEditFieldTwo.text?.toDouble())!)
                launchDialog()
            }else{
                
            }
            break
        default:
            break
        }
    }
    @IBAction func mMenRadioButton(_ sender: Any) {
        print("Men Checked")
        mMan = true
        mWoman = false
        mMenRadioButton.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_white)), for: .normal)
        
        mWomanRadioButton.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_white)), for: .normal)
    }
    @IBAction func mWomanRadioButton(_ sender: Any) {
        print("Woman Checked")
        mWoman = true
        mMan = false
    mWomanRadioButton.setImage(UIImage(named: "ic_select")?.tint(with: UIColor(netHex: Colors.color_white)), for: .normal)
        
        mMenRadioButton.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_white)), for: .normal)
    }
    
    func setColors(){
        mWomanRadioButton.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_white)), for: .normal)
        
        mMenRadioButton.setImage(UIImage(named: "ic_unselect")?.tint(with: UIColor(netHex: Colors.color_white)), for: .normal)
    }
    
}

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

extension UIImage {
    func tint(with color: UIColor) -> UIImage {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        
        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension Double {
    func toString() -> String {
        return String(format: "%.2f",self)
    }
}


extension UIButton
{
    func applyGradient(colors: [CGColor])
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        gradientLayer.frame = self.bounds
        gradientLayer.cornerRadius = self.layer.cornerRadius

        self.layer.addSublayer(gradientLayer)
    }
}
