//
//  SplashScreenViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/2/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit
import FLAnimatedImage

class SplashScreenViewController: UIViewController {

    var counter = 0
    var timer = Timer()

    @IBOutlet weak var mAnimateImage: FLAnimatedImageView!
    
    @IBOutlet weak var mImageSplash: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadGifsProgress()
        delayLaunchIntroductionView()
        mImageSplash.image = UIImage(named:"img_logo_mhegas_opt.jpg")
    }
    
    func loadGifsProgress(){
        let imageData = try! Data(contentsOf: Bundle.main.url(forResource: "loadedapp", withExtension: "gif")!)
        mAnimateImage.animatedImage = FLAnimatedImage(animatedGIFData: imageData)
    }
    
    func launchHome(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.barStyle = .black
               self.navigationController?.navigationBar.barTintColor = .black
               self.navigationController?.navigationBar.tintColor = .black
    }
    
    func delayLaunchIntroductionView(){
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(actionDelayHome), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelayHome() {
        counter += 1
        if(counter == 10){
            timer.invalidate()
            launchHome()
        }
    }
    
}
