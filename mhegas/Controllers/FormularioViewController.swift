//
//  FormularioViewController.swift
//  mhegas
//
//  Created by Charls Salazar on 07/04/20.
//  Copyright © 2020 avento. All rights reserved.
//

import UIKit

class FormularioViewController: UIViewController {
    @IBOutlet weak var mHeaderBanner: UIView!
    @IBOutlet weak var mTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBottomCorners()
        mTitleLabel.text = "Ingrese los datos para\n realizar las operaciones"
    }
    
    func setBottomCorners(){
        mHeaderBanner.clipsToBounds = true
        mHeaderBanner.layer.cornerRadius = 30
        mHeaderBanner.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Change status bar color
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.barTintColor = .black
        self.navigationController?.navigationBar.tintColor = .black
    }
}
