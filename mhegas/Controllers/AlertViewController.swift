//
//  AlertViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/29/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {

    @IBOutlet weak var mAlertLegendLabel: UILabel!
    @IBOutlet weak var mAlertImageView: UIImageView!
    
    @IBOutlet weak var mDialogContainerView: UIView!
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    
    var mLegend : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        setColorDefault(mImage: mAlertImageView)
        mAlertLegendLabel.text = mLegend
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .default
    }
    
    @IBAction func mAcceptButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setColorDefault(mImage : UIImageView){
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor.white
    }
}

extension AlertViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}
