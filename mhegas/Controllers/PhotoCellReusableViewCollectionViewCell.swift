//
//  PhotoCellReusableViewCollectionViewCell.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/24/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class PhotoCellReusableViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mImage: UIImageView!
    
    
}
