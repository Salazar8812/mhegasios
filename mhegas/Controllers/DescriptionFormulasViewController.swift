//
//  DescriptionFormulasViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/29/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class DescriptionFormulasViewController: UIViewController {
    @IBOutlet weak var mBookImageView: UIImageView!
    
   
    @IBOutlet weak var mContentDescritionLabel: UILabel!
    @IBOutlet weak var mDialogContainerView: UIView!
    @IBOutlet weak var mTitleLabel: UILabel!
    
    @IBOutlet weak var mHeighDescription: NSLayoutConstraint!
    @IBOutlet weak var mBannerConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var mBannerHeaderView: UIView!
    @IBOutlet weak var mHeightConstraint: NSLayoutConstraint!
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    
    var mTitle: String  = ""
    var mContentDescription : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true

        setColorDefault(mImage: mBookImageView)
        mTitleLabel.text = mTitle
        mContentDescritionLabel.text = mContentDescription
        
        let cHeight = requiredHeight(labelText: mContentDescritionLabel.text!)
        let cHeightTitle = requiredHeight(labelText: mTitleLabel.text!)
        
        if ((requiredHeight(labelText: mContentDescritionLabel.text!)) > 400 ){
            mHeightConstraint.constant =  cHeight + 100
            mHeighDescription.constant = requiredHeight(labelText: mContentDescritionLabel.text!) - 100
        }else{
            if(cHeightTitle < 60){
                mHeighDescription.constant = requiredHeight(labelText: mContentDescritionLabel.text!) + 50
                mHeightConstraint.constant = requiredHeight(labelText: mContentDescritionLabel.text!) + 270
            }else{
                mHeighDescription.constant = requiredHeight(labelText: mContentDescritionLabel.text!) + 70
                mHeightConstraint.constant = requiredHeight(labelText: mContentDescritionLabel.text!) + 300
            }
        }
        
        mBannerConstraintHeight.constant = 120
        mDialogContainerView.layoutIfNeeded()
        mBannerHeaderView.layoutIfNeeded()
        mContentDescritionLabel.layoutIfNeeded()
        
    }
    
    func requiredHeight(labelText:String) -> CGFloat {
        
        let font = UIFont(name: "Helvetica", size: 16.0)
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: .max))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = labelText
        label.sizeToFit()
        return label.frame.height
        
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .default
    }

    
    @IBAction func mAcceptButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setColorDefault(mImage : UIImageView){
        mImage.image = mImage.image?.withRenderingMode(.alwaysTemplate)
        mImage.tintColor = UIColor(netHex: Colors.color_white)
    }
    
}

extension DescriptionFormulasViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}
