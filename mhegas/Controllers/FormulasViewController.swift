//
//  FormulasViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class FormulasViewController: UIViewController,TableViewCellClickDelegate {
    var mDataSource : BaseDataSource<NSObject, ItemFormulaBaseTableViewCell>?
    var mListFomulas : [Formulas] = []
    @IBOutlet weak var mSearchImageView: UIImageView!
    
    @IBOutlet weak var mFormulasTableView: UITableView!
    
    @IBOutlet weak var mSearchTextField: UITextField!
    
    var mListAux : [Formulas] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateFormulas()
        mDataSource = BaseDataSource(tableView: self.mFormulasTableView, delegate: self)
        mDataSource?.setHeightRow(height:65)
        mDataSource?.update(items: mListFomulas)
        
        setTintcolor()
        
        addTarget()
    }
    
    func addTarget (){
        mSearchTextField.addTarget(self, action: #selector(watcher(_:)), for: .editingChanged)
    }
    
    func setTintcolor(){
        mSearchImageView.image = mSearchImageView.image?.withRenderingMode(.alwaysTemplate)
        //mSearchImageView.tintColor = UIColor(netHex: Colors.color_background_app)
    }
    
    func onTableViewCellClick(item: NSObject, cell: UITableViewCell) {
        let mFormula = item as! Formulas
        if(mFormula.mIdFormula == "666"){
            launchSpecialController()
        }else{
            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FormsCalculateViewController") as! FormsCalculateViewController
            loginVC.mFormulas = (item as! Formulas)
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    }
    
    func launchSpecialController(){
        let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CalculateNewOperationViewController") as! CalculateNewOperationViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @objc func watcher(_ textfield : UITextField){
        var cadAux : String = ""
        cadAux = textfield.text!
        mListAux.removeAll()
        if(cadAux.count == 0 || cadAux == ""){
            mDataSource!.update(items: mListFomulas)
        }else{
            for item in mListFomulas{
                if(item.mTipoFormula.uppercased().contains(cadAux.uppercased())){
                    mListAux.append(item)
                }
            }
            mDataSource!.update(items: mListAux)
        }
    }
    
    func populateFormulas(){
        
        
        mListFomulas.append(Formulas(mTipoFormula: "Diágnostico Ácido-Base", mNombreFormula: "Peso predicho para SDRA (ARDSnet)", mIdFormula: "1",mParams: ["pCO2","HCO3"],mFunctionCalculate: "concentracionPlasmatica",mDescription: StringDescriptions.description_form_empty));
        
        
        mListFomulas.append(Formulas(mTipoFormula: "Concentración plasmática de Hidrogeniones [H+ ]", mNombreFormula: "Peso predicho para SDRA (ARDSnet)", mIdFormula: "1",mParams: ["pCO2","HCO3"],mFunctionCalculate: "concentracionPlasmatica",mDescription: StringDescriptions.description_form_empty));
        mListFomulas.append(Formulas(mTipoFormula: "PCO2 esperado para Acidosis Metabólica", mNombreFormula: "IMC", mIdFormula: "1",mParams: ["HCO3"],mFunctionCalculate: "acidosisMetabolica",mDescription: StringDescriptions.description_form_empty));
        mListFomulas.append(Formulas(mTipoFormula: "PCO2 esperado para Alcalosis Metabólica", mNombreFormula: "Distensibilidad pulmonar estática (Cst compliance estática)", mIdFormula: "1",mParams: ["HCO3"],mFunctionCalculate: "alcalosisMetabolica", mDescription: StringDescriptions.description_form_empty));
        mListFomulas.append(Formulas(mTipoFormula: "Anion Gap", mNombreFormula: "Anion Gap", mIdFormula: "1",mParams: ["Na","Cl","HCO3"],mFunctionCalculate: "anion", mDescription: StringDescriptions.description_form_empty));
        mListFomulas.append(Formulas(mTipoFormula: "Anion Gap corregido en hipoalbuminemia", mNombreFormula: "Presión trans-vía aerea", mIdFormula: "1",mParams: ["Anion Gap*","albumina dL*"],mFunctionCalculate: "anionGap", mDescription: StringDescriptions.description_form_empty));
        mListFomulas.append(Formulas(mTipoFormula: "Osmolalidad efectiva ó Tonicidad plasmática", mNombreFormula: "Resistencia de la vía aerea (Raw)", mIdFormula: "1",mParams: ["Na","Glucosa"],mFunctionCalculate: "osmolalidad", mDescription: StringDescriptions.description_form_empty));
        mListFomulas.append(Formulas(mTipoFormula: "Déficit de bicarbonato", mNombreFormula: "CO2e para Acidosis Metabólica", mIdFormula: "1",mParams: ["Bicarbonato normal","Peso"],mFunctionCalculate: "deficitBicarbonato",mDescription: StringDescriptions.description_form_empty));
        mListFomulas.append(Formulas(mTipoFormula: "Déficit de Cl", mNombreFormula: "CO2e para Alcalosis Metabólica", mIdFormula: "1",mParams: ["Peso","Cl plasmático"],mFunctionCalculate: "deficitCI",mDescription: StringDescriptions.description_form_empty));
        mListFomulas.append(Formulas(mTipoFormula: "Déficit de H+", mNombreFormula: "Ajuste de la FR para el CO2 deseado", mIdFormula: "1",mParams: ["Peso","HCO3 Plasma"],mFunctionCalculate: "defitcitHplus", mDescription: StringDescriptions.description_form_empty));
      
        mListFomulas.append(Formulas(mTipoFormula: "Exceso de hiato aniónico / Déficit de bicarbonato", mNombreFormula: "Presión de distensión pulmonar (Driving pressure)", mIdFormula: "1",mParams: ["Hiato aniónico","HCO3"],mFunctionCalculate: "hiatoAnionico", mDescription: StringDescriptions.description_form_empty));
        
         mListFomulas.append(Formulas(mTipoFormula: "More Fórmulas", mNombreFormula: "More Fórmulas", mIdFormula: "666",mParams: ["Exmaple"],mFunctionCalculate: "hiatoAnionico", mDescription: StringDescriptions.description_form_empty));
        

    }
    
}
