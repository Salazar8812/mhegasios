//
//  AlertSpecialValidationsViewController.swift
//  mhegas
//
//  Created by Charls Salazar on 5/14/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

protocol ContinueValidataDelegate : NSObjectProtocol{
    func OnAccept(mViewController:  UIViewController, mOpenNewCalculate: String)
}

class AlertSpecialValidationsViewController: UIViewController {
    var customBlurEffectStyle: UIBlurEffect.Style!
    var customInitialScaleAmmount: CGFloat!
    var customAnimationDuration: TimeInterval!
    @IBOutlet weak var mDialogContainerView: UIView!
    @IBOutlet weak var mDescriptionLabel: UILabel!
    var mDescriptionDialog  : String = ""
    var mOpenNewCalculate : String = ""
    var mContinueValidataDelegate : ContinueValidataDelegate!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationCapturesStatusBarAppearance = true
        mDescriptionLabel.text = mDescriptionDialog
    }

    func getDelegates(mContinueValidataDelegate: ContinueValidataDelegate){
        self.mContinueValidataDelegate = mContinueValidataDelegate
    }
    
    @IBAction func mAcceptButton(_ sender: Any) {
        mContinueValidataDelegate.OnAccept(mViewController:self, mOpenNewCalculate:mOpenNewCalculate)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .default
    }
    
}

extension AlertSpecialValidationsViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mDialogContainerView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}
