//
//  HomeViewController.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/10/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, ImageCarouselDelegate {
    
    @IBOutlet weak var mCarouselA: iCarousel!
    @IBOutlet weak var mCarouselB: iCarousel!
    var mImagePreviewCarouselA : CarouselDataSource!
    var mImagePreviewCarouselB : CarouselDataSource!

    var mListImageA : [String] = []
    var mListImageB : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        mImagePreviewCarouselA = CarouselDataSource(carrusel: mCarouselA,mImageIndicatorDelegate: self)
        mImagePreviewCarouselB = CarouselDataSource(carrusel: mCarouselB,mImageIndicatorDelegate: self)
        
        mImagePreviewCarouselA.settings()
        mImagePreviewCarouselB.settings()
        
        popualteImageCarouselA()
        popualteImageCarouselB()
    }
    
    func popualteImageCarouselA(){
        mListImageA.append("diapositiva1")
        mListImageA.append("diapositiva2")
        mListImageA.append("diapositiva3")
        mListImageA.append("diapositiva4")
        mListImageA.append("diapositiva5")
        mListImageA.append("diapositiva6")
        mListImageA.append("diapositiva7")
        mImagePreviewCarouselA.update(items: mListImageA)
        
    }

    func popualteImageCarouselB(){
        mListImageB.append("algoritmo.JPG")
        mListImageB.append("cuadro.JPG")
        mListImageB.append("d1.JPG")
        mListImageB.append("d2.JPG")
        mListImageB.append("d3.JPG")
        mListImageB.append("d4.JPG")
        mListImageB.append("d5.JPG")
        mListImageB.append("d6.JPG")
        mListImageB.append("d7.JPG")
        mListImageB.append("d8.JPG")
        mListImageB.append("d9.JPG")
        mListImageB.append("d10.JPG")
        mListImageB.append("d11.JPG")
        mImagePreviewCarouselB.update(items: mListImageB)
    }

    func OnSelectedImage(mItemSelected: String) {
        
    }
    
    func launchPreview(mPhotoCLicked: String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: OperationResultViewController.self))
        
        let v1 = storyboard.instantiateViewController(withIdentifier: "ImagesVisorViewController") as! ImagesVisorViewController
       v1.mPhotoCLicked = mPhotoCLicked
        MIBlurPopup.show(v1, on: self)
    }
    
}
