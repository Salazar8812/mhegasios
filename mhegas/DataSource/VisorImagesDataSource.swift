//
//  VisorImagesDataSource.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/1/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

protocol ImageIndicatorDelegate: NSObjectProtocol{
    func OnSelectedImage(mItemSelected:  String)
}

class VisorImagesDataSource: NSObject, iCarouselDataSource, iCarouselDelegate {
    
    var carrusel : iCarousel!
    var mItemsImages : [String] = []
    public var mSelectedIndex : Int = 0
    var mImageIndicatorDelegate : ImageIndicatorDelegate!
    
    init(carrusel: iCarousel, mImageIndicatorDelegate : ImageIndicatorDelegate){
        self.carrusel = carrusel
        self.mImageIndicatorDelegate = mImageIndicatorDelegate
    }
    
    func settings(){
        carrusel.type = iCarouselType.linear
        carrusel.delegate = self
        carrusel.dataSource = self
    }
    
    func update(items: [String]){
        mItemsImages = items
        carrusel.reloadData()
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return mItemsImages.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var view : ItemImageCollectionReusableView?
        
        view = (Bundle.main.loadNibNamed("ItemImageCollectionReusableView", owner: self, options: nil)![0] as! ItemImageCollectionReusableView)
        
        view?.mItemImageView.image = UIImage(named: mItemsImages[index])
        
        return view!
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
    self.mImageIndicatorDelegate.OnSelectedImage(mItemSelected: mItemsImages[index])
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        mSelectedIndex = carousel.currentItemIndex
    }
    
}
