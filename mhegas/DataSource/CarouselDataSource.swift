//
//  CarouselDataSource.swift
//  mhegas
//
//  Created by Charls Salazar on 6/13/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

protocol ImageCarouselDelegate: NSObjectProtocol{
    func OnSelectedImage(mItemSelected:  String)
}

class CarouselDataSource:NSObject, iCarouselDataSource, iCarouselDelegate {
    
    var carrusel : iCarousel!
    var mItemsImages : [String] = []
    public var mSelectedIndex : Int = 0
    var mImageIndicatorDelegate : ImageCarouselDelegate!
    
    init(carrusel: iCarousel, mImageIndicatorDelegate : ImageCarouselDelegate){
        self.carrusel = carrusel
        self.mImageIndicatorDelegate = mImageIndicatorDelegate
    }
    
    func settings(){
        carrusel.type = iCarouselType.linear
        carrusel.delegate = self
        carrusel.dataSource = self
    }
    
    func update(items: [String]){
        mItemsImages = items
        carrusel.reloadData()
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return mItemsImages.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var view : ItemCarouselCollectionViewCell?
        
        view = (Bundle.main.loadNibNamed("ItemCarouselCollectionViewCell", owner: self, options: nil)![0] as! ItemCarouselCollectionViewCell)
        
        view?.mImageCarousel.image = UIImage(named: mItemsImages[index])
        
        return view!
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        self.mImageIndicatorDelegate.OnSelectedImage(mItemSelected: mItemsImages[index])
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        mSelectedIndex = carousel.currentItemIndex
    }
    
}
