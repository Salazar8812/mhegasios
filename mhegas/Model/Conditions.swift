//
//  Conditions.swift
//  mhegas
//
//  Created by Charls Salazar on 5/13/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class Conditions: NSObject {
    
    var mpH: Double?
    var mpaCO2 : Double?
    var mBase : Double?
    var mBaseEsperada : Double?
    var mHCO3 : Double?
    var mpaCO2Esperado : Double?
    
    var mArrayConditions : [Conditions] = []
    
    init(mpH: Double, mpaCO2: Double, mBase: Double, mBaseEsperada: Double, mHCO3: Double, mpaCO2Esperado:Double) {
        self.mpH = mpH
        self.mpaCO2 = mpH
        self.mBase = mBase
        self.mBaseEsperada = mBaseEsperada
        self.mHCO3 = mHCO3
        self.mpaCO2Esperado = mpaCO2Esperado
    }
    
    func createValidations(){
        mArrayConditions.append(Conditions(mpH: 7.35,mpaCO2: 35.0,mBase: 0.0,mBaseEsperada: 0.0,mHCO3: 0.0,mpaCO2Esperado: 0.0))
    }
}
