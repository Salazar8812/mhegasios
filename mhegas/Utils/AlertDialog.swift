//
//  AlertDialog.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/3/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

public class AlertDialog {
    
    public static var overlay : UIView?
    public static var viewController : UIViewController?
    
    public static func show(title: String, body: String, view : UIViewController, handler: ((UIAlertAction) -> Swift.Void)? = nil){
        let refreshAlert = UIAlertController(title: title, message: body, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        view.present(refreshAlert, animated: true, completion: nil)
    }
    
    @objc public static func pressed(){
        
    }
    
}
