//
//  CardView.swift
//  mhegas
//
//  Created by Charls Salazar on 07/04/20.
//  Copyright © 2020 avento. All rights reserved.
//
import UIKit

class CardView: UIView {

    var radius: CGFloat = 2

    override func layoutSubviews() {
        layer.cornerRadius = radius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius)

        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 3);
        layer.shadowOpacity = 0.5
        layer.shadowPath = shadowPath.cgPath
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
