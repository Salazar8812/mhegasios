//
//  StringDescriptions.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 4/26/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class StringDescriptions: NSObject {
    
    static let description_form_empty = ""
    
    static let description_form1 = "7,35 a 7,45"
    
    static let description_form2 = "35 - 45 mmHg a nivel del mar"
    
    static let description_form3 = "20-24 mEq/L"
   
    static let description_form4 = "a) Disminución del pH = Acidosis"

    static let description_form5 = "b) Aumento del pH = Alcalosis"

    static let description_form6 = "a) Aumento de PCO2: Acidosis respiratoria "
   
    static let description_form7 = "b) Disminucion de PCO2: Alcalosis respiratoria"
    
    static let description_form8 = "a) Disminucion del HCO3: Acidosis metabolica"

    static let description_form9 = "b) Aumento del HCO3: Alkalosis metabolica"
    
    static let description_form10 = "En la hipercapnia aguda el bicarbonato plasmático aumenta 1 mEq/L por cada 10 mmHg de aumento en la pCO2. \n" +
        "\nEn la hipercapnia crónica el bicarbonato plasmático aumenta 3-4 mEq/L por cada 10 mmHg de aumento de la pCO2 \n" +
       "\nTamponamiento intracelular. La hemoglobina, los fosfatos y las proteínas liberan hidrogeniones [H+ ] que se unen al HCO3– para forma H2CO3, consiguiendo disminuir 2 mEq/l de HCO3– por cada 10 mmHg que desciende la pCO2."
    
    
    static let description_form11 = "En la hipercapnia crónica el bicarbonato plasmático aumenta 3-4 mEq/L por cada 10 mmHg de aumento de la pCO2"
    
    static let description_form12 = "Tamponamiento intracelular. La hemoglobina, los fosfatos y las proteínas liberan hidrogeniones [H+ ] que se unen al HCO3– para forma H2CO3, consiguiendo disminuir 2 mEq/l de HCO3– por cada 10 mmHg que desciende la pCO2."
    
    static let description_result_one = "ACIDOSIS METABOLICA + ALCALOSIS RESPIRATORIA"
    static let description_result_two = "ACIDOSIS METABOLICA + ACIDOSIS RESPIRATORIA"
    static let description_result_three = "ALCALOSIS METABÓLICA + ALCALOSIS RESPIRATORIA"
    static let description_result_four = "ALCALOSIS METABÓLICA + ACIDOSIS RESPIRATORIA"
    static let description_result_five = "ACIDOSIS RESPIRATORIA AGUDA"
    static let description_result_six = "RESPIRATORIA + ALCALOSIS METABÓLICA agregada"
    static let description_result_seven = "ACIDOSIS RESPIRATORIA + ACIDOSIS METABÓLICA agregada"
    static let description_result_eight = "ALCALOSIS RESPIRATORIA AGUDA"
    static let description_result_nine = "ALCALOSIS RESPIRATORIA + ALCALOSIS METABÓLICA agregada"
    static let description_result_ten = "ALCALOSIS RESPIRATORIA + ACIDOSIS METABÓLICA agregada"

    static let description_result_eleven = "Anion Gap elevado"
    static let description_result_twelve = "Anion Gap normal"
    
    static let description_result_thirteen = "ACIDOSIS HIPERCLORÉMICA agregada"
    static let description_result_fourteen = "ACIDOSIS METABÓLICA DE ANION GAP NORMAL agregada"
    static let description_result_fiveteen = "ACIDOSIS METABOLICA DE ANION GAP ELEVADO pura"
    static let description_result_sixteen = "ALCALOSIS METABÓLICA agregada ó Compensación de acidosis respiratoria preexistente"
}
