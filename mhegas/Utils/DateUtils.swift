//
//  DateUtils.swift
//  orientacionmedica
//
//  Created by Charls Salazar on 5/2/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

open class DateUtils: NSObject {
    
    open static var dateFormat : String = "yyyy/MM/dd"
    open static var dateFormatN : String = "dd/MM/yyyy"
    
    open class func toString(date : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    
    open class func getDateNow() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    
    open class func toDate(date : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.date(from: date)!
    }
    
    open class func toString(format : String, date : Date) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        return formatter.string(from: date)
    }
    
    open class func addingDays(date : Date, value: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: value, to: date)!
    }
    
    open class func getDate(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        
        guard let date = dateFormatter.date(from: date) else {
            assert(false, "no date from string")
            return ""
        }
        
        dateFormatter.dateFormat = "yyyy/MM/dd"
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let timeStamp = dateFormatter.string(from: date)
        
        return timeStamp
    }
    
}
