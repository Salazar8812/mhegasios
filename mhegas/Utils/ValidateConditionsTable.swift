//
//  ValidateConditionsTable.swift
//  mhegas
//
//  Created by Charls Salazar on 5/13/19.
//  Copyright © 2019 avento. All rights reserved.
//

import UIKit

class ValidateConditionsTable: NSObject {
    
    func PCO2Esperado(HCO3: Double)-> Double{
        let mAux = HCO3 * 1.5
        let mResult =  mAux + 8
        return mResult
    }
    
    func BaseEsperada(PaCO2 : Double)->Double{
        let mAux = PaCO2 - 40
        let mResult = mAux * 0.4
        return mResult
    }
    
    func AnionGap(Na : Double, Cl: Double, HCO3: Double)->Double{
        let mAux = Cl + HCO3
        let mResult = Na - mAux
        return mResult
    }
    
    func DeltadeAG(AnionGap : Double)->Double{
        let mResult = AnionGap - 12
        return mResult
    }
    
    func DeltadeHCO3(HCO3: Double)->Double{
        let mResult = 24 - HCO3
        return mResult
    }

    
}
